package com.tw.biblioteca;

import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class InputTest {

    @Test
    public void readLineFromFileShouldReturnCorrectString() throws IOException {
        BufferedReader bufferedReaderForFile = mock(BufferedReader.class);
        BufferedReader bufferedReaderForBooks = mock(BufferedReader.class);
        BufferedReader bufferedReaderForConsole = mock(BufferedReader.class);
        BufferedReader bufferedReaderForUser = mock(BufferedReader.class);
        when(bufferedReaderForBooks.readLine()).thenReturn("Absolution Gap, Alastair Reynolds , 1990");
        Input input = new Input(bufferedReaderForBooks, bufferedReaderForFile, bufferedReaderForUser, bufferedReaderForConsole);
        Assert.assertEquals("Absolution Gap, Alastair Reynolds , 1990", input.readLineFromBookFile().readLine());
    }

    @Test
    public void readLineFromConsoleShouldReturnCorrectString() throws IOException {
        BufferedReader bufferedReaderForConsole = mock(BufferedReader.class);
        BufferedReader bufferedReaderForBooks = mock(BufferedReader.class);
        BufferedReader bufferedReaderForFile = mock(BufferedReader.class);
        BufferedReader bufferedReaderForUser = mock(BufferedReader.class);

        Input input = new Input(bufferedReaderForBooks, bufferedReaderForFile, bufferedReaderForUser, bufferedReaderForConsole);
        when(bufferedReaderForConsole.readLine()).thenReturn("Hello");
        Assert.assertEquals("Hello", input.readLineFromConsole());
    }
}
