package com.tw.biblioteca;

import org.junit.Test;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class MenuTest {

    @Test
    public void displayContentShouldInvokeConsoleOutputDisplay() throws IOException {
        Input input = mock(Input.class);
        ConsoleOutput consoleOutput = mock(ConsoleOutput.class);
        BookList bookList = mock(BookList.class);
        Library library = mock(Library.class);
        Menu menu = new Menu(consoleOutput, input, library, "1231234");
        when(input.readLineFromConsole()).thenReturn("Air");
        menu.displayContent("2", bookList);
        verify(consoleOutput).display("Enter Name of the book : ");
    }

    @Test
    public void displayContentShouldInvokeDisplayOption3() throws IOException {
        Input input = mock(Input.class);
        ConsoleOutput consoleOutput = mock(ConsoleOutput.class);
        BookList bookList = mock(BookList.class);
        Library library = mock(Library.class);
        Menu menu = new Menu(consoleOutput, input, library, "1231234");
        when(input.readLineFromConsole()).thenReturn("Air");
        menu.displayContent("3", bookList);
        verify(consoleOutput).display("Enter Name of the book : ");
    }

    @Test
    public void displayContentShouldInvokeDisplayOption4() throws IOException {
        Input input = mock(Input.class);
        ConsoleOutput consoleOutput = mock(ConsoleOutput.class);
        BookList bookList = mock(BookList.class);
        Library library = mock(Library.class);
        Menu menu = new Menu(consoleOutput, input, library, "1231234");
        when(input.readLineFromConsole()).thenReturn("Air");
        menu.displayContent("4", bookList);
        verify(consoleOutput).display("Start typing the name: ");
    }

    @Test
    public void displayContentShouldInvokeDisplayOption6() throws IOException {
        Input input = mock(Input.class);
        ConsoleOutput consoleOutput = mock(ConsoleOutput.class);
        BookList bookList = mock(BookList.class);
        Library library = mock(Library.class);
        Menu menu = new Menu(consoleOutput, input, library, "1231234");
        when(input.readLineFromConsole()).thenReturn("Air");
        menu.displayContent("6", bookList);
        verify(consoleOutput).display("Enter Name of the movie : ");
    }
}
