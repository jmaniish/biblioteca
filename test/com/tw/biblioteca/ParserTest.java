package com.tw.biblioteca;

import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ParserTest {

    @Test
    public void parseListOfBooksShouldReturnListOfPopulatedBooks() throws IOException {
        Parser parser = new Parser();
        BufferedReader bufferedReader = mock(BufferedReader.class);
        when(bufferedReader.readLine()).thenReturn("fox,abd,123").thenReturn(null);
        Assert.assertEquals(1, parser.populateBookDetails(bufferedReader).size());
    }

    @Test
    public void parseListOfBooksShouldReturnListOfPopulatedMovies() throws IOException {
        Parser parser = new Parser();
        BufferedReader bufferedReader = mock(BufferedReader.class);
        when(bufferedReader.readLine()).thenReturn("fox,abd,123,8.8").thenReturn(null);
        Assert.assertEquals(1, parser.populateMovieDetails(bufferedReader).size());
    }

    @Test
    public void parseListOfBooksShouldReturnListOfPopulatedUsers() throws IOException {
        Parser parser = new Parser();
        BufferedReader bufferedReader = mock(BufferedReader.class);
        when(bufferedReader.readLine()).thenReturn("fox,abd,123,acb,man").thenReturn(null);
        Assert.assertEquals(1, parser.populateUserDetails(bufferedReader).size());
    }
}
