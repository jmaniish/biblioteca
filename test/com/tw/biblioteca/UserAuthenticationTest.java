package com.tw.biblioteca;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserAuthenticationTest {

    @Test
    public void validateShouldReturnLibraryNumberWhenItIsThereInSystem() {
        List<User> users = mock(List.class);
        HashMap<String, String> credentials = mock(HashMap.class);
        UserAuthentication userAuthentication = new UserAuthentication(users, credentials);
        when(credentials.get("123-1234")).thenReturn("password");
        Assert.assertEquals("123-1234", userAuthentication.validate("123-1234", "password"));
    }

}
