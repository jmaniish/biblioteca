package com.tw.biblioteca;

import org.junit.Test;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class BibliotecaTest {
    @Test
    public void startShouldInvokeDisplayMenu() throws IOException {
        Input input = mock(Input.class);
        ConsoleOutput consoleOutput = mock(ConsoleOutput.class);
        BookList bookList = mock(BookList.class);
        Menu menu = mock(Menu.class);
        Library library = mock(Library.class);
        Biblioteca biblioteca = new Biblioteca(consoleOutput, input, library, bookList, menu);
        when(input.readLineFromConsole()).thenReturn("0");
        biblioteca.start();
        verify(menu).displayMenu();
    }


}
