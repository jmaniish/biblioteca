package com.tw.biblioteca;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class BookListTest {

    @Test
    public void searchBookShouldReturnListOfMatchingBook() {
        List<Book> books = new ArrayList<>();
        Book book = new Book("Fire Fighter", "Ram Sampath", "1990");
        Book book1 = new Book("Fire Fighter 1", "Ram Sampath", "1992");
        Book book2 = new Book("Fire Fighter 2", "Ram Sampath", "1994");
        books.add(book1);
        books.add(book2);
        books.add(book);
        BookList bookList = new BookList(books);
        Assert.assertEquals(1, bookList.searchBook("1").size());
    }
}
