package com.tw.biblioteca;

import org.junit.Test;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class SessionTest {

    @Test
    public void isLoginSuccessfulShouldCallValidateToCheckUserNamePassword() throws IOException, FormatNotCorrectException {
        ConsoleOutput consoleOutput = mock(ConsoleOutput.class);
        Input consoleInput = mock(Input.class);
        UserAuthentication userAuthentication = mock(UserAuthentication.class);
        Session session = new Session(consoleOutput, consoleInput, userAuthentication);
        when(consoleInput.readLineFromConsole()).thenReturn("123-1234").thenReturn("password");
        session.isLoginSuccessful();
        verify(userAuthentication).validate("123-1234", "password");
    }
}
