package com.tw.biblioteca;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;

public class LibraryTest {

    @Test
    public void checkOutShouldReturnTrueIfBookIsRemoved() {
        List<Book> books = new ArrayList<>();
        List<Movie> movies = mock(ArrayList.class);
        Book book = new Book("Fire Fighter", "Ram Sampath", "1990");
        Book book1 = new Book("Fire Fighter 1", "Ram Sampath", "1992");
        Book book2 = new Book("Fire Fighter 2", "Ram Sampath", "1994");
        books.add(book1);
        books.add(book2);
        books.add(book);
        Library library = new Library(books, movies);
        Assert.assertNotNull(library.checkoutBook("Fire Fighter", "1231234"));
    }

    @Test
    public void returnBookShouldReturnFalseIfBookDoesNotBelongToLibrary() {
        List<Book> books = new ArrayList<>();
        List<Movie> movies = mock(ArrayList.class);
        Book book = new Book("Fire Fighter", "Ram Sampath", "1990");
        Book book1 = new Book("Fire Fighter 1", "Ram Sampath", "1992");
        Book book2 = new Book("Fire Fighter 2", "Ram Sampath", "1994");
        books.add(book1);
        books.add(book2);
        books.add(book);
        Library library = new Library(books, movies);
        library.checkoutBook("Fire Fighter", "1231234");
        Assert.assertFalse(library.returnBook("Fire", "1231234"));
    }

    @Test
    public void returnBookShouldReturnFalseIfBookExistButWrongUser() {
        List<Book> books = new ArrayList<>();
        List<Movie> movies = mock(ArrayList.class);
        Book book = new Book("Fire Fighter", "Ram Sampath", "1990");
        Book book1 = new Book("Fire Fighter 1", "Ram Sampath", "1992");
        Book book2 = new Book("Fire Fighter 2", "Ram Sampath", "1994");
        books.add(book1);
        books.add(book2);
        books.add(book);
        Library library = new Library(books, movies);
        library.checkoutBook("Fire Fighter", "1231234");
        Assert.assertFalse(library.returnBook("Fire Fighter", "1231235"));
    }

    @Test
    public void returnBookShouldReturnTrueIfBookIsAddedToTheLibrary() {
        List<Book> books = new ArrayList<>();
        List<Movie> movies = mock(ArrayList.class);
        Book book = new Book("Fire Fighter", "Ram Sampath", "1990");
        Book book1 = new Book("Fire Fighter 1", "Ram Sampath", "1992");
        Book book2 = new Book("Fire Fighter 2", "Ram Sampath", "1994");
        books.add(book1);
        books.add(book2);
        books.add(book);
        Library library = new Library(books, movies);
        library.checkoutBook("Fire Fighter", "1231234");
        Assert.assertTrue(library.returnBook("Fire Fighter", "1231234"));
    }

    @Test
    public void checkOutShouldReturnTrueIfMovieIsRemoved() {
        List<Book> books = mock(ArrayList.class);
        List<Movie> movies = new ArrayList<>();
        Movie movie = new Movie("Fire Fighter", "Ram Sampath", "1990","8.0");
        Movie movie1 = new Movie("Fire Fighter 1", "Ram Sampath", "1992","9.0");
        Movie movie2 = new Movie("Fire Fighter 2", "Ram Sampath", "1994","9.2");
        movies.add(movie);
        movies.add(movie1);
        movies.add(movie2);
        Library library = new Library(books, movies);
        Assert.assertNotNull(library.checkoutMovie("Fire Fighter"));
    }

}
