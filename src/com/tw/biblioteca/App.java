package com.tw.biblioteca;

import java.io.*;
import java.util.HashMap;
import java.util.List;

//application class
class App {
    public static void main(String[] args) throws IOException, FormatNotCorrectException {
        Input input = getConsoleInput();
        Parser parser = new Parser();
        ConsoleOutput consoleOutput = new ConsoleOutput();
        List<Book> books = parser.populateBookDetails(input.readLineFromBookFile());
        List<Movie> movies = parser.populateMovieDetails(input.readLineFromMovieFile());
        Library library = new Library(books, movies);
        List<User> users = parser.populateUserDetails(input.readLineFromUserFile());
        HashMap<String, String> credentials = parser.populateUserCredentials();
        UserAuthentication userAuthentication = new UserAuthentication(users, credentials);
        Session session = new Session(consoleOutput, input, userAuthentication);
        String libraryNumber;
        if ((libraryNumber = session.isLoginSuccessful()) == null) {
            return;
        }
        BookList bookList = new BookList(books);
        Menu menu = new Menu(consoleOutput, input, library, libraryNumber);
        Biblioteca biblioteca = new Biblioteca(consoleOutput, input, library, bookList, menu);
        biblioteca.start();
    }

    private static Input getConsoleInput() throws FileNotFoundException {
        BufferedReader bufferedReaderForBooks = new BufferedReader(new FileReader("BookList.txt"));
        BufferedReader bufferedReaderForMovies = new BufferedReader(new FileReader("MovieList.txt"));
        BufferedReader bufferedReaderForUser = new BufferedReader(new FileReader("UserList.txt"));
        BufferedReader bufferedReaderForConsole = new BufferedReader(new InputStreamReader(System.in));
        return new Input(bufferedReaderForBooks, bufferedReaderForMovies,
                bufferedReaderForUser, bufferedReaderForConsole);
    }
}
