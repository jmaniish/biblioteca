package com.tw.biblioteca;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// understands user authentication in the app
public class UserAuthentication {

    List<User> users;
    Map<String, String> credentials;

    public UserAuthentication(List<User> users, HashMap<String, String> credentials) {
        this.users = users;
        this.credentials = credentials;
    }

    String validate(String libraryNumber, String password) {

        try {
            if (password.equals(credentials.get(libraryNumber))) {
                return libraryNumber;
            }
            throw new FormatNotCorrectException();
        } catch (FormatNotCorrectException e) {
            System.out.println("Library number format not correct");
        }
        return null;
    }
}
