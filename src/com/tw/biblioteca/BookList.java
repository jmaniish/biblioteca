package com.tw.biblioteca;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

//understands search of books
public class BookList {

    private List<Book> books;

    BookList(List<Book> books) {
        this.books = books;
    }

    ArrayList<Book> searchBook(String next) {
        ArrayList<Book> booksFound = books.stream().filter(book -> book.getName().
                toUpperCase().contains(next.toUpperCase())
                || book.getAuthor().toUpperCase().
                contains(next.toUpperCase())).
                collect(Collectors.toCollection(ArrayList::new));
        booksFound.forEach(System.out::println);
        return booksFound;
    }
}
