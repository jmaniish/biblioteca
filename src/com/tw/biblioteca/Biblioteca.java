package com.tw.biblioteca;

import java.io.IOException;

//understands flow of application
class Biblioteca {

    private final String WELCOME_MESSAGE = "Welcome customer";
    private ConsoleOutput consoleOutput;
    private Input input;
    private Library library;
    private BookList bookList;
    private Menu menu;

    Biblioteca(ConsoleOutput consoleOutput, Input input, Library library, BookList bookList, Menu menu) {
        this.consoleOutput = consoleOutput;
        this.input = input;
        this.library = library;
        this.bookList = bookList;
        this.menu = menu;
    }

    void start() throws IOException {
        consoleOutput.display(WELCOME_MESSAGE);
        while (true) {
            menu.displayMenu();
            String optionSelected = input.readLineFromConsole();
            if (optionSelected.equals("0")) {
                consoleOutput.display("Quitting Application.");
                break;
            }
            menu.displayContent(optionSelected, bookList);
            consoleOutput.display("");
        }
    }
}

