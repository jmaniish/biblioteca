package com.tw.biblioteca;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//understands creation of objects from file
class Parser {

    HashMap<String, String> credentials = new HashMap<>();

    List<Book> populateBookDetails(BufferedReader bufferedReader) throws IOException {
        List<Book> books = new ArrayList<>();
        String currentLine;
        while ((currentLine = bufferedReader.readLine()) != null) {
            String tokens[] = currentLine.split(",");
            books.add(new Book(tokens[0], tokens[1], tokens[2]));
        }
        return books;
    }

    List<Movie> populateMovieDetails(BufferedReader bufferedReader) throws IOException {
        ArrayList<Movie> movies = new ArrayList<>();
        String currentLine;
        while ((currentLine = bufferedReader.readLine()) != null) {
            String tokens[] = currentLine.split(",");
            movies.add(new Movie(tokens[0], tokens[1], tokens[2], tokens[3]));
        }
        return movies;
    }

    List<User> populateUserDetails(BufferedReader bufferedReader) throws IOException {
        ArrayList<User> users = new ArrayList<>();

        String currentLine;
        while ((currentLine = bufferedReader.readLine()) != null) {
            String tokens[] = currentLine.split(",");
            users.add(new User(tokens[0], tokens[1], tokens[2], tokens[3], tokens[4]));
            credentials.put(tokens[0], tokens[4].trim());
        }
        return users;
    }

    HashMap<String, String> populateUserCredentials() throws IOException {
        return credentials;
    }
}
