package com.tw.biblioteca;

import java.io.BufferedReader;
import java.io.IOException;

//understands different input medium
public class Input {

    BufferedReader bufferedReaderForBook;
    BufferedReader bufferedReaderForMovie;
    BufferedReader bufferedReaderForUser;
    BufferedReader bufferedReaderForConsole;

    public Input(BufferedReader bufferedReaderForBook, BufferedReader bufferedReaderForMovie,
                 BufferedReader bufferedReaderForUser, BufferedReader bufferedReaderForConsole) {
        this.bufferedReaderForBook = bufferedReaderForBook;
        this.bufferedReaderForConsole = bufferedReaderForConsole;
        this.bufferedReaderForMovie = bufferedReaderForMovie;
        this.bufferedReaderForUser = bufferedReaderForUser;
    }

    BufferedReader readLineFromBookFile() throws IOException {
        return bufferedReaderForBook;
    }

    BufferedReader readLineFromMovieFile() throws IOException {
        return bufferedReaderForMovie;
    }

    BufferedReader readLineFromUserFile() throws IOException {
        return bufferedReaderForUser;
    }

    String readLineFromConsole() throws IOException {
        return bufferedReaderForConsole.readLine();
    }
}
