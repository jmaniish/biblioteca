package com.tw.biblioteca;

import java.io.IOException;
import java.util.List;

//understands different options available to user
class Menu {

    private final String DISPLAY_BOOKS = "1";
    private final String CHECKOUT_BOOK = "2";
    private final String RETURN_BOOK = "3";
    private final String SEARCH_BOOK = "4";
    private final String DISPLAY_MOVIES = "5";
    private final String CHECKOUT_MOVIE = "6";
    private ConsoleOutput consoleOutput;
    private Input input;
    private Library library;
    private String libraryNumber;

    Menu(ConsoleOutput consoleOutput, Input input, Library library, String libraryNumber) {
        this.consoleOutput = consoleOutput;
        this.input = input;
        this.library = library;
        this.libraryNumber = libraryNumber;
    }

    void displayMenu() {
        consoleOutput.display("Select one from below");
        consoleOutput.display("1. List Books");
        consoleOutput.display("2. Checkout Book");
        consoleOutput.display("3. Return Book");
        consoleOutput.display("4. Search Book");
        consoleOutput.display("5. List Movies");
        consoleOutput.display("6. Checkout Movie");
        consoleOutput.display("Press zero to Quit\n");
    }

    void displayContent(String optionSelected, BookList list) throws IOException {

        String name;
        switch (optionSelected) {
            case DISPLAY_BOOKS:
                List<Book> books = library.displayListOfBooks();
                consoleOutput.display("Displaying List of Movies");
                consoleOutput.display(String.format("%-51s %-30s %-15s %s", "NAME", "AUTHOR", "YEAR", "RATING"));
                for (Book book : books) {
                    consoleOutput.display(book.toString());
                }
                break;

            case CHECKOUT_BOOK:
                consoleOutput.display("Enter Name of the book : ");
                name = input.readLineFromConsole();
                if (library.checkoutBook(name.trim(), libraryNumber) != null) {
                    consoleOutput.display("Thank you! Enjoy the book");
                    break;
                }
                consoleOutput.display("That book is not available.");
                break;

            case RETURN_BOOK:
                consoleOutput.display("Enter Name of the book : ");
                name = input.readLineFromConsole();
                if (library.returnBook(name.trim(), libraryNumber)) {
                    consoleOutput.display("Thank you for returning the book.");
                    break;
                }
                consoleOutput.display("That is not a valid book to return.");
                break;

            case SEARCH_BOOK:
                consoleOutput.display("Start typing the name: ");
                list.searchBook(input.readLineFromConsole());
                break;

            case DISPLAY_MOVIES:
                List<Movie> movies = library.displayListOfMovies();
                consoleOutput.display("Displaying List of Movies");
                consoleOutput.display(String.format("%-51s %-30s %-15s %s", "NAME", "AUTHOR", "YEAR", "RATING"));
                for (Movie movie : movies) {
                    consoleOutput.display(movie.toString());
                }
                break;

            case CHECKOUT_MOVIE:
                consoleOutput.display("Enter Name of the movie : ");
                name = input.readLineFromConsole();
                if (library.checkoutMovie(name.trim()) != null) {
                    consoleOutput.display("Thank you! Enjoy the movie");
                    break;
                }
                consoleOutput.display("That movie is not available.");
                break;

            default:
                consoleOutput.display("Select a valid option");
                break;
        }
    }
}
