package com.tw.biblioteca;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// represents list of books
class Library {

    private List<Book> books;
    private List<Movie> movies;
    private Map<Book, String> checkedoutBooks;

    Library(List<Book> books, List<Movie> movies) {
        this.books = books;
        this.movies = movies;
        checkedoutBooks = new HashMap<>();
    }

    List<Book> displayListOfBooks() {
        return books;
    }

    Book checkoutBook(String name, String libraryNumber) {
        for (int i = 0; i < books.size(); i++) {
            Book book = books.get(i);
            if (book.hasName(name)) {
                checkedoutBooks.put(book, libraryNumber);
                return books.remove(i);
            }
        }
        return null;
    }

    boolean returnBook(String name, String libraryNumber) {
        for (Book book : checkedoutBooks.keySet()) {
            if (book.hasName(name)) {
                if (libraryNumber == checkedoutBooks.get(book)) {
                    {
                        books.add(book);
                        checkedoutBooks.remove(book);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    List<Movie> displayListOfMovies() {
        return movies;
    }

    Movie checkoutMovie(String name) {
        for (int i = 0; i < movies.size(); i++) {
            Movie movie = movies.get(i);
            if (movie.hasName(name)) {
                return movies.remove(i);
            }
        }
        return null;
    }
}