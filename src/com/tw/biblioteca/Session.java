package com.tw.biblioteca;

import java.io.IOException;

//understands logging in into application
class Session {

    private ConsoleOutput consoleOutput;
    private Input input;
    private UserAuthentication userAuthentication;

    Session(ConsoleOutput consoleOutput, Input input, UserAuthentication userAuthentication) {
        this.consoleOutput = consoleOutput;
        this.input = input;
        this.userAuthentication = userAuthentication;
    }

    String isLoginSuccessful() throws IOException, FormatNotCorrectException {
        consoleOutput.display("Enter library number: ");
        String libraryNumber = input.readLineFromConsole();
        libraryNumber = libraryNumber.trim();

        consoleOutput.display("Enter password: ");
        String password = input.readLineFromConsole();

        return userAuthentication.validate(libraryNumber.trim(), password.trim());
    }
}
