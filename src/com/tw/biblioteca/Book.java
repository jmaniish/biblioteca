package com.tw.biblioteca;

public class Book {

    private String name;
    private String author;
    private String yearPublished;

    public Book(String name, String author, String yearPublished) {
        this.name = name;
        this.author = author;
        this.yearPublished = yearPublished;
    }

    String getName() {
        return name;
    }

    String getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return String.format("%-50s %-30s %s", name, author, yearPublished);
    }

    boolean hasName(String name) {
        return name != null ? name.toUpperCase().equals(this.name.toUpperCase()) : false;
    }
}
