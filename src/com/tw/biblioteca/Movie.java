package com.tw.biblioteca;

class Movie {

    private String name;
    private String director;
    private String year;
    private String rating;


    Movie(String name, String director, String year, String rating) {
        this.name = name;
        this.director = director;
        this.year = year;
        this.rating = rating;
    }

    @Override
    public String toString() {
        return String.format("%-50s %-30s %-15s %s", name, director, year, rating);
    }

    boolean hasName(String name) {
        return name != null ? name.toUpperCase().equals(this.name.toUpperCase()) : false;
    }
}
